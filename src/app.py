import logging

from flask import Flask, request, json, jsonify

from src.entities.ClientEntitie import Client
from src.entities.ClientEntitieCleaner import ClientCleaner
from src.entities.MockEntitie import Data
from src.entities.UserEntitie import User
from src.entities.UserEntitieCleaner import UserCleaner
from src.exceptions.Exceptions import AttributeTypeMismatch

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.logger.setLevel(logging.ERROR)


@app.route("/")
def home():
    return jsonify(isError=False, message="Success", statusCode=200, data="API IS WORKING"), 200


@app.route("/hi")
def hello():
    return jsonify(isError=False, message="Success", statusCode=200, data="HELLO DJIBRIL"), 200


@app.route('/create_user', methods=['POST'])
def create_user():
    try:
        body = request.get_json()
        data = Data(**body)
        response = {"msg": "user created with success !", "user_data": json.loads(data.toJson())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except AttributeTypeMismatch as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400

    except Exception as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400


@app.route('/process_user', methods=['POST'])
def process_user():
    try:
        body = request.get_json()
        data = User(**body)
        data = UserCleaner(data).transform()
        response = {"msg": "user processed with success !", "user_data": json.loads(data.toJson())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except AttributeTypeMismatch as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400

    except Exception as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400


@app.route('/create_client', methods=['POST'])
def create_client():
    try:
        body = request.get_json()
        data = Client(**body)
        response = {"msg": "user created with success !", "user_data": json.loads(data.json())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except Exception as e:
        app.logger.error(str(e))
        error_trace = json.loads(e.json()) if e.__class__.__name__ == 'ValidationError' else str(e)
        return jsonify(isError=True, message="Failure", statusCode=400, traceback=error_trace), 400


@app.route('/process_client', methods=['POST'])
def process_client():
    try:
        body = request.get_json()
        data = Client(**body)
        data = ClientCleaner(data).transform()
        response = {"msg": "user created with success !", "user_data": json.loads(data.json())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except Exception as e:
        app.logger.error(str(e))
        error_trace = json.loads(e.json()) if e.__class__.__name__ == 'ValidationError' else str(e)
        return jsonify(isError=True, message="Failure", statusCode=400, traceback=error_trace), 400


if __name__ == "__main__":
    app.run(host='0.0.0.0')
