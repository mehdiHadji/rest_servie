import json

from src.validators.attributes_checker import auto_attr_check


@auto_attr_check
class Data:
    message = str
    user = str

    def __init__(self, message, user):
        self.message = message
        self.user = user

    def __str__(self):
        return f"<<{self.message}>><<{self.user}>>"

    def toDict(self) -> dict:
        return {"message": self.message, "user": self.user}

    def toJson(self):
        return json.dumps(self.toDict())



