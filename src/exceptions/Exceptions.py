class AttributeTypeMismatch(Exception):

    def __init__(self, name, type_):
        self.name = name
        self.type_ = type_
        self.message = f"<{name}> attribute must be set to an instance of {type_}"

    def __str__(self):
        return self.message


class AttributeMissingException(Exception):
    def __init__(self):
        self.message = "One or more required attributes were missing from the request."

    def __str__(self):
        return self.message


class EmailFormatException(Exception):
    def __init__(self, **kwargs):
        self.message = "Invalid email adress"

    def __str__(self):
        return self.message


class PhoneNumberFormatException(Exception):
    def __init__(self):
        self.message = "Invalid phone number"

    def __str__(self):
        return self.message
