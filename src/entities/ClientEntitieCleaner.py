from src.cleaners.global_cleaners import capitalize_str, uppercase_str

from src.cleaners.specific_cleaners import (
    compute_age,
    infer_gender,
    is_eligible_age,
    extract_country_code,
    extract_country_name
)
from src.entities.ClientEntitie import Client


class ClientCleaner:
    client: Client

    def __init__(self, client):
        self.client = client

    def transform(self) -> Client:
        self.client.first_name = capitalize_str(self.client.first_name)
        self.client.last_name = uppercase_str(self.client.last_name)
        self.client.age = compute_age(self.client.birth_date)
        self.client.gender = infer_gender(self.client.gender)
        self.client.is_eligible = is_eligible_age(self.client.age)
        self.client.country_code = extract_country_code(self.client.country)
        self.client.country_name = extract_country_name(self.client.country)
        return self.client
